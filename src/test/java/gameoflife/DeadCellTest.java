package gameoflife;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class DeadCellTest {

	@Test
	void shouldBecomeAliveWithThreeNeighbours() {
		var cell = new DeadCell();
		assertThat(cell.evolve(NumberOfNeigbours.THREE)).isInstanceOf(AliveCell.class);
	}
	
	@Test
	void shouldStayDiedWithTwoNeighbours() {
		var cell = new DeadCell();
		assertThat(cell.evolve(NumberOfNeigbours.TWO)).isInstanceOf(DeadCell.class);
	}

}
