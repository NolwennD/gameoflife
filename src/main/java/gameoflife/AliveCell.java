package gameoflife;

public final class AliveCell implements Cell {

	@Override
	public Cell evolve(NumberOfNeigbours numberOfNeighbours) {
		return 
				switch (numberOfNeighbours) {
					case TWO, THREE -> new AliveCell();
					default -> new DeadCell();
			};
	}

}
