package gameoflife;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Grid {

	private Map<Coordinate, Cell> cells;

	public Grid(Entry<Coordinate, Cell>... entry) {
		this.cells = Arrays.stream(entry)
				.collect(Collectors.toMap(Entry::getKey, Entry::getValue));
	}

	public NumberOfNeigbours getAliveCells(Coordinate coordinate) {
		List<Coordinate> neighbourCoords = createNeighbours(coordinate);
		long count = neighbourCoords.stream()
					.map(e -> cells.getOrDefault(e, new DeadCell()))
					.filter(e -> e instanceof AliveCell)
					.count();
		return 
			switch ((int) count) {
			case 2 -> NumberOfNeigbours.TWO;
			case 3 -> NumberOfNeigbours.THREE;
			default -> NumberOfNeigbours.OTHER;
			};
	}

	private List<Coordinate> createNeighbours(Coordinate coordinate) {
		List<Coordinate> neighbourCoords = List.of(
				Coordinate.of(coordinate.x() - 1, coordinate.y()),
				Coordinate.of(coordinate.x() - 1, coordinate.y() + 1),
				Coordinate.of(coordinate.x() - 1, coordinate.y() - 1),
				Coordinate.of(coordinate.x() + 1, coordinate.y()),
				Coordinate.of(coordinate.x() + 1, coordinate.y() + 1),
				Coordinate.of(coordinate.x() + 1, coordinate.y() - 1),
				Coordinate.of(coordinate.x(), coordinate.y() + 1),
				Coordinate.of(coordinate.x(), coordinate.y() - 1)
				);
		return neighbourCoords;
	}

}
