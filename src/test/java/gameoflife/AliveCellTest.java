package gameoflife;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class AliveCellTest {

	@Test
	void should_die_without_neighbour() {
		AliveCell aliveCell = new AliveCell();
		assertThat(aliveCell.evolve(NumberOfNeigbours.OTHER)).isInstanceOf(DeadCell.class);
	}
	
	@Test
	void should_stay_alive_with_two_neighbours() {
		AliveCell aliveCell = new AliveCell();
		assertThat(aliveCell.evolve(NumberOfNeigbours.TWO)).isInstanceOf(AliveCell.class);
	}
	
	@Test
	void should_stay_alive_with_three_neighbours() {
		AliveCell aliveCell = new AliveCell();
		assertThat(aliveCell.evolve(NumberOfNeigbours.THREE)).isInstanceOf(AliveCell.class);
	}

}
