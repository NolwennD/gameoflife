package gameoflife;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;

class GridTest {

	@Test
	void should_have_zero_alive_cell_when_is_empty() {
		Grid grid = new Grid();
		assertThat(grid.getAliveCells(Coordinate.of(0,0))).isEqualTo(NumberOfNeigbours.OTHER);
	}
	
	@Test
	void should_have_one_alive_cell() {
		Cell aliveCell = new AliveCell();
		Entry<Coordinate, Cell> entry = Map.entry(Coordinate.of(0,1), aliveCell);
		Grid grid = new Grid(entry);
		assertThat(grid.getAliveCells(Coordinate.of(0,0))).isEqualTo(NumberOfNeigbours.OTHER);
	}
	
	@Test
	void should_have_two_alive_cell() {
		Cell aliveCell = new AliveCell();
		Entry<Coordinate, Cell> entry1 = Map.entry(Coordinate.of(0,1), aliveCell);
		Entry<Coordinate, Cell> entry2 = Map.entry(Coordinate.of(1,1), aliveCell);
		Grid grid = new Grid(entry1, entry2);
		assertThat(grid.getAliveCells(Coordinate.of(0,0))).isEqualTo(NumberOfNeigbours.TWO);
	}
	
	@Test
	void should_have_three_alive_cell() {
		Cell aliveCell = new AliveCell();
		Entry<Coordinate, Cell> entry1 = Map.entry(Coordinate.of(0,1), aliveCell);
		Entry<Coordinate, Cell> entry2 = Map.entry(Coordinate.of(1,1), aliveCell);
		Entry<Coordinate, Cell> entry3 = Map.entry(Coordinate.of(1,0), aliveCell);
		Grid grid = new Grid(entry1, entry2, entry3);
		assertThat(grid.getAliveCells(Coordinate.of(0,0))).isEqualTo(NumberOfNeigbours.THREE);
	}

}
