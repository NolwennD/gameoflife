package gameoflife;

public sealed interface Cell
permits DeadCell, AliveCell {
	public Cell evolve(NumberOfNeigbours numberOfNeighbours);
}
