package gameoflife;

public final class DeadCell implements Cell {

	@Override
	public Cell evolve(NumberOfNeigbours numberOfNeighbours) {
		return 
			switch (numberOfNeighbours) {
				case THREE -> new AliveCell();
				default -> new DeadCell();
		};
	}
}
